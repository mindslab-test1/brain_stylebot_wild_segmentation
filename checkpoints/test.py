r'''
    PointRend Test Script
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai

    Practical script that gets one input and results corresponding prediction.
'''

### Import detectron2
import os
import detectron2
## setup logger
from detectron2.utils.logger import setup_logger
setup_logger()
## import some common libraries
import numpy as np
import cv2
import torch
## import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2.data import MetadataCatalog
from detectron2.structures import BitMasks, Boxes, BoxMode, Keypoints, PolygonMasks, RotatedBoxes
from detectron2.data.datasets import register_coco_instances
## import PointRend project
#from detectron2.projects import point_rend

register_coco_instances("stylebot", {}, "/datasets/stylebot_test/stylebot.json", "/datasets/stylebot_test")
metadata = MetadataCatalog.get("stylebot")

import sys; sys.path.insert(1, "detectron2_repo/projects/PointRend")
import point_rend
import pickle as pkl

def setup(args):
    """
    Create configs and perform basic setups.
    """
    cfg = get_cfg()
    add_pointrend_config(cfg)
    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    cfg.freeze()
    default_setup(cfg, args)
    return cfg

class GO(object):
    def __init__(self, args):
        self.args = args
        self.cfg = setup(args)
        self.model = DefaultTrainer.build_model(self.cfg)
        DetectionCheckpointer(self.model, save_dir=self.cfg.OUTPUT_DIR).resume_or_load(
            self.cfg.MODEL.WEIGHTS, resume=self.args.resume
        )

if __name__ == "__main__":
    cfg = get_cfg()
    point_rend.add_pointrend_config(cfg)
    cfg.merge_from_file("detectron2_repo/projects/PointRend/configs/InstanceSegmentation/pointrend_rcnn_R_50_FPN_1x_coco.yaml")
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
    cfg.MODEL.WEIGHTS = "output/model_final.pth"
    predictor = DefaultPredictor(cfg)

    imdir = '/DATA1/stylebot_wild_test'
    image_list = [x for x in os.listdir(imdir) if (x.lower().endswith(".png") or x.lower().endswith(".jpg"))
                  and not x.lower().startswith("vis_")]
    for image_path in image_list[:]:
        image = cv2.imread(os.path.join(imdir, image_path), cv2.IMREAD_COLOR)
        #image = cv2.cvtColor(cv2.imread(imdir), cv2.COLOR_BGR2RGB)

        outputs = predictor(image)

        # following results visualized results
        v = Visualizer(image[:, :, ::-1], metadata, scale=1.0, instance_mode=ColorMode.IMAGE_BW)
        prediction = outputs["instances"].to("cpu")

        boxes = prediction.pred_boxes if prediction.has("pred_boxes") else None
        if isinstance(boxes, Boxes) or isinstance(boxes, RotatedBoxes):
            boxes = boxes.tensor.numpy()
        else:
            boxes = np.asarray(boxes)

        scores = np.asarray(prediction.scores) if prediction.has("scores") else None
        classes = np.asarray(prediction.pred_classes) if prediction.has("pred_classes") else None
        keypoints = np.asarray(prediction.pred_keypoints) if prediction.has("pred_keypoints") else None

        if prediction.has("pred_masks"):
            masks = np.asarray(prediction.pred_masks)
        else:
            masks = None

        point_rend_result = v.draw_instance_predictions(prediction).get_image()
        total_dict = dict()
        for idx, (box, score, cls_id, mask) in enumerate(zip(boxes, scores, classes, masks)):
            item_dict = dict()
            item_dict['box'] = box
            item_dict['score'] = score
            item_dict['cls_id'] = cls_id
            item_dict['mask'] = mask
            total_dict[idx] = item_dict

        with open(os.path.join(imdir, image_path[:-4] + ".pkl"), 'wb') as f:
            pkl.dump(total_dict, f)

        cv2.imwrite(os.path.join(imdir, 'vis_' + image_path), point_rend_result[:, :, ::-1])



