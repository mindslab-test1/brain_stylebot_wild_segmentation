r'''
    Author: Sungguk Cha
    eMail:  sungguk@midnslab.ai
    add category names
'''

from OmegaConf import omegaconf

import json
import os
import yaml

opj = os.path.join

r'''
    args requires:
        ddir (str): basedir
'''
args = omegaconf.load('./config.yaml')

filename = opj(args.ddir, 'stylebot.json')
with open(filename, 'r') as f:
    annotations = json.load(filename)

annotations['']
