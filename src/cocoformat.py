r'''
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    bitmask to coco json format converter
'''

import numpy as np                                 # (pip install numpy)
from skimage import measure                        # (pip install scikit-image)
from shapely.geometry import Polygon, MultiPolygon # (pip install Shapely)

from omegaconf import OmegaConf
from pathlib import Path
from PIL import Image, ImageDraw

import argparse
import json
import os
import yaml

opj = os.path.join

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str)
    return parser.parse_args()

r'''
    args requires
        ddir (str): dataset basedir
'''

args = OmegaConf.load(get_args().config)

#
# Loading given dataset
#

paths = list()
basedir = args.ddir
for path in Path(basedir).rglob('*.json'):
    path = str(path)
    if 'stylebot.json' in path: continue
    r'''
    if '_seg' in path: continue
    if '_bitmask' in path:  continue
    if '.json' in path: continue
    if '.yaml' in path: continue
    '''
    paths.append(path)
print(len(paths), "json paths are loaded")

#
# json 2 bitmasks
#

def poly2mask(mask, poly, class_id, index):
    if len(poly) <= 2: return mask
    color = '#{:02x}{:02x}{:02x}'.format(0, class_id, index)
    poly = [np.floor(x) for x in poly]
    try:
        ImageDraw.Draw(mask).polygon(poly, fill=color)
    except:
        print(poly)
        raise RuntimeError("c8")
    return mask
def json2mask(_json):
    _json = _json.replace('\\', '/')
    image_dir = '/'.join(_json.split('/')[:-1])
    save_dir = image_dir.replace("images", "annotations")
    name = _json.split('/')[-1].split('.')[0]
    os.makedirs(save_dir, exist_ok=True)
    jsonf = OmegaConf.load(_json)
    imgdir = opj(image_dir, jsonf["style"]["pair_id"])
    img = Image.open(imgdir).convert('RGB')
     
    class_ids = dict()   
    mask = Image.new('RGB', img.size)
    for d in jsonf["style"]["items"]:
        class_id = int(d["category_id"])
        polys = d["segmentation"]
        if class_id in class_ids:
            index = class_ids[class_id]
            class_ids[class_id] += 1
        else:
            index = 0
            class_ids[class_id] = 1
        for poly in polys:
            mask = poly2mask(mask, poly, class_id, index)
    mask.save(opj(save_dir, name + '.png'))
 
for path in paths:
    json2mask(path)

#
# bitmask2coco
#

def create_sub_masks(mask_image):
    width, height = mask_image.size
 
    # Initialize a dictionary of sub-masks indexed by RGB colors
    sub_masks = {}
    for x in range(width):
        for y in range(height):
            # Get the RGB values of the pixel
            pixel = mask_image.getpixel((x,y))[:3]
 
            # If the pixel is not black...
            if pixel != (0, 0, 0):
                # Check to see if we've created a sub-mask...
                pixel_str = str(pixel)
                sub_mask = sub_masks.get(pixel_str)
                if sub_mask is None:
                   # Create a sub-mask (one bit per pixel) and add to the dictionary
                    # Note: we add 1 pixel of padding in each direction
                    # because the contours module doesn't handle cases
                    # where pixels bleed to the edge of the image
                    sub_masks[pixel_str] = Image.new('1', (width+2, height+2))
 
                # Set the pixel value to 1 (default is 0), accounting for padding
                sub_masks[pixel_str].putpixel((x+1, y+1), 1)
 
    return sub_masks
 
def create_sub_mask_annotation(sub_mask, image_id, category_id, annotation_id, is_crowd):
    # Find contours (boundary lines) around each sub-mask
    # Note: there could be multiple contours if the object
    # is partially occluded. (E.g. an elephant behind a tree)
    contours = measure.find_contours(np.asarray(sub_mask), 0.5, positive_orientation='low')
 
    segmentations = []
    polygons = []
    for contour in contours:
        # Flip from (row, col) representation to (x, y)
        # and subtract the padding pixel
        for i in range(len(contour)):
            row, col = contour[i]
            contour[i] = (col - 1, row - 1)
 
        # Make a polygon and simplify it
        poly = Polygon(contour)
        poly = poly.simplify(1.0, preserve_topology=False)
        polygons.append(poly)
        segmentation = np.array(poly.exterior.coords).ravel().tolist()
        segmentations.append(segmentation)
 
    # Combine the polygons to calculate the bounding box and area
    multi_poly = MultiPolygon(polygons)
    x, y, max_x, max_y = multi_poly.bounds
    width = max_x - x
    height = max_y - y
    bbox = (x, y, width, height)
    area = multi_poly.area
 
    annotation = {
        'segmentation': segmentations,
        'iscrowd': is_crowd,
        'image_id': image_id,
        'category_id': category_id,
        'id': annotation_id,
        'bbox': bbox,
        'area': area
    }
 
    return annotation

annotations = list()
passes = list()
image_id=1
annotation_id=1
annotations = list()
image_ids = dict()
for path in paths:
    _mask = path[:-4] + '_bitmask.png'
    try:
        mask = Image.open(_mask)
        sub_masks = create_sub_masks(mask)
        for color, sub_mask in sub_masks.items():
            category_id = color[1]
            annotation = create_sub_mask_annotation(sub_mask, image_id, category_id, annotation_id, 0)
            annotations.append(annotation)
            annotation_id += 1
        image_ids[path] = image_id
        image_id += 1
        print(path)
        exit()
    except:
        passes.append(path)

if image_id == 1:
    raise RuntimeError("No mask loaded")

#
# To json file
#

style = dict()
 
info = dict()
info["description"] = "Stylebot Dataset"
info["contributor"] = "Brain-Vision, MINDs Lab"
info["date_created"] = "Dec, 15, 2020"
style["info"] = info
 
images = list()
for path in paths:
    if path in passes: continue
    _json = path
    img = Image.open(_json)
    width, height = img.size
     
    image = dict()
    image["filename"] = _json
    image["height"] = height
    image["width"] = width
    image["id"] = image_ids[path]
    images.append(image)
style["images"] = images
 
style["annotations"] = annotations
 
with open(opj(basedir, 'stylebot.json'), 'w') as f:
    json.dump(style, f)

with open(opj(basedir, 'passes.yaml'), 'w') as f:
    yaml.dump(passes, f)

print('{} images and annotations are successfully saved'.format(len(image_ids)))
