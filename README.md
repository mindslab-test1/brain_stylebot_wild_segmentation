#Stylebot Segmentation Engine with detectron2/Mask\_R-CNN+PointRend
Author: Sungguk Cha \
eMail:  sungguk@mindslab.ai 

This repository builds Stylebot segmentation engine using FAIR/detectron2/maskRCNN+PointRend.
The docker runs a server containing following functions.

1. Training the model.
2. Prediction. Given an input image, it results the models output as it is for later use (e.g., keypoint estimation). In other words, the client should handle the result.

### Requirements

docker ≥ 19.03

## Updates

## How to Use

### Preprocess
For data preprocessing (using custom dataset), please refer to (cocoformatter)[TBD] repository.

### Building docker image
``` shell
cd docker
docker build -t segmentation:v0 .
```

### Launch
Run `docker/launch.sh'.
``` bash
dataset="/path/to/dataset"
checkpoint="/path/to/checkpoint"
docker run --gpus all -it \
    --shm-size=8gb --env="DISPLAY" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume=$dataset':/datasets/stylebot' \
    --volume=$checkpoint':/checkpoints/' \
    --name=segmentation segmentation:v0
```



## Server Docker Usage
This section might be corresponded only to `hksong-review` branch.

### Registry TAG
`docker.maum.ai:443/brain/stylebot_wild_seg:v1.0.0-server`

Note that there is another engine including segmentation module for stylebot.
It would be glad if you merge these into single engine :)

### Docker RUN Command
```shell script
docker run -itd --ipc=host --gpus="all" -v /path/to/checkpoint_dir:/stylebot_seg/pretrained -p 50100:50100\
 --name stylebot_wild_seg docker.maum.ai:443/brain/stylebot_wild_seg:v1.0.0-server
```

you may update an environment for `checkpoint_path` at run command.
`-e CHECKPOINT_PATH=/stylebot_seg/pretrained/my_own_weight.pth`
