r'''
    PointRend Test Script
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai

    Practical script that gets one input and results corresponding prediction.
'''

### Import detectron2
import os
import detectron2
## setup logger
from detectron2.utils.logger import setup_logger
setup_logger()
## import some common libraries
import numpy as np
import cv2
import torch
## import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2.data import MetadataCatalog
from detectron2.structures import BitMasks, Boxes, BoxMode, Keypoints, PolygonMasks, RotatedBoxes
from detectron2.data.datasets import register_coco_instances
from detectron2.evaluation import COCOEvaluator
from typing import List


register_coco_instances("stylebot", {}, "/datasets/stylebot_wild_test_aggregated_coco.json", "/datasets")
metadata = MetadataCatalog.get("stylebot")

## import PointRend project
import sys; sys.path.insert(1, "detectron2_repo/projects/PointRend")
import point_rend
import pickle as pkl


def get_all_inputs_outputs(model, data_loader):
    for data in data_loader:
        yield data, model(data)



class Model():
    def __init__(self, yaml_path="detectron2_repo/projects/PointRend/configs/InstanceSegmentation/pointrend_rcnn_R_50_FPN_1x_coco.yaml",
                 weight_path="output/model_final.pth"):
        """
        Create configs and perform basic setups.
        """
        cfg = get_cfg()
        point_rend.add_pointrend_config(cfg)
        cfg.merge_from_file(yaml_path)
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
        cfg.MODEL.WEIGHTS = weight_path
        self.predictor = DefaultPredictor(cfg)
        self.evaluator = COCOEvaluator("stylebot", tasks="segm", distributed=False)

    def _get_result_from_nparray(self, image_np: np.ndarray) -> object:
        self.evaluator.reset()
        for inputs, outputs in get_all_inputs_outputs(self.predictor, [image_np]):
            self.evaluator.process(inputs, outputs)
        eval_results = self.evaluator.evaluate()
        print(eval_results)
        output = outputs["instances"].to("cpu")
        return output

    def _get_result_from_binary(self, image_binary: bytes) -> object:
        image = cv2.imdecode(np.frombuffer(image_binary, dtype=np.uint8), cv2.IMREAD_COLOR)

        evaluator.reset()
        for inputs, outputs in get_all_inputs_outputs(self.predictor, [image]):
            evaluator.process(inputs, outputs)
        eval_results = evaluator.evaluate()
        print(eval_results)
        output = outputs["instances"].to("cpu")
        return output

    def _get_result_from_filepath(self, file_path: str) -> object:
        image = cv2.imread(file_path, cv2.IMREAD_COLOR)
        output = self.predictor(image)

        

        output = output["instances"].to("cpu")
        return output

    def _get_result_from_directory(self, dir_path: str) -> List[object]:
        image_list = [x for x in os.listdir(dir_path) if (x.lower().endswith(".png") or x.lower().endswith(".jpg"))
                      and not x.lower().startswith("vis_")]

        outputs = []
        for image_path in image_list:
            outputs.append(self._get_result_from_filepath(image_path))

        return outputs

    def get_segmentation_result(self, image: object) -> List[object]:
        if isinstance(image, str):  # by file path or directory path
            if os.path.isdir(image):
                seg_result = self._get_result_from_directory(image)
            else:
                seg_result = [self._get_result_from_filepath(image)]

        elif isinstance(image, bytearray):  # safe coding for bytearray
            image = bytes(image)
            seg_result = [self._get_result_from_binary(image)]
        elif isinstance(image, bytes):  # convert for bytes image
            seg_result = [self._get_result_from_binary(image)]
        elif isinstance(image, np.ndarray):
            seg_result = [self._get_result_from_nparray(image)]
        else:
            raise TypeError("image should be either str, bytes, or numpy array")

        return seg_result

    def convert_output_dict(self, output) -> List[dict]:
        boxes = output.pred_boxes if output.has("pred_boxes") else None
        if isinstance(boxes, Boxes) or isinstance(boxes, RotatedBoxes):
            boxes = boxes.tensor.numpy()
        else:
            boxes = np.asarray(boxes)

        scores = np.asarray(output.scores) if output.has("scores") else None
        classes = np.asarray(output.pred_classes) if output.has("pred_classes") else None

        if output.has("pred_masks"):
            masks = np.asarray(output.pred_masks)
        else:
            masks = None

        total_dict = []
        for idx, (box, score, cls_id, mask) in enumerate(zip(boxes, scores, classes, masks)):
            item_dict = dict()
            item_dict['box'] = box
            item_dict['score'] = score
            item_dict['cls_id'] = cls_id
            item_dict['mask'] = mask
            total_dict.append(item_dict)

        return total_dict

if __name__ == "__main__":
    servicer = Model()
    imdir = '/DATA1/stylebot_wild_test'

    image_list = [x for x in os.listdir(imdir) if (x.lower().endswith(".png") or x.lower().endswith(".jpg"))
                  and not x.lower().startswith("vis_")]
    outputs = servicer.get_segmentation_result(image_list)

    for idx, image_path in enumerate(image_list[:]):
        image = cv2.imread(os.path.join(imdir, image_path), cv2.IMREAD_COLOR)
        pred_output = outputs[idx]
        item_list = servicer.convert_output_dict(pred_output)

        with open(os.path.join(imdir, image_path[:-4] + ".pkl"), 'wb') as f:
            pkl.dump(item_list, f)

        v = Visualizer(image[:, :, ::-1], metadata, scale=1.0, instance_mode=ColorMode.IMAGE_BW)
        point_rend_result = v.draw_instance_predictions(pred_output).get_image()
        cv2.imwrite(os.path.join(imdir, 'vis_' + image_path), point_rend_result[:, :, ::-1])