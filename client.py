from proto.segmentation_pb2 import Input, Point, BoundingBox, SegItem, SegResult
from proto.segmentation_pb2_grpc import ImageSegStub

import argparse
import grpc
import os
from pathlib import Path

import csv
from tqdm import tqdm

CHUNK_SIZE = 1024 * 1024  # 1MB

CSV_COLUMN_FORMAT = ["image_path", "bitmask_path", "class_id(1~13)", "confidence"]
class Client:
    def __init__(self, ip="localhost", port=50100):
        self.server_ip = ip
        self.server_port = port
        self.stub = ImageSegStub(
            grpc.insecure_channel(
                self.server_ip + ":" + str(self.server_port)
            )
        )

    def _generate_binary_iterator(self, binary):
        for idx in range(0, len(binary), CHUNK_SIZE):
            yield Input(image=binary[idx:idx+CHUNK_SIZE])

    def get_service(self, image_path, result_dir="bitmask"):
        result_dir = Path(result_dir)
        with open(image_path, 'rb') as rf:
            image_byte = rf.read()
        image_iterator = self._generate_binary_iterator(image_byte)
        grpc_output = self.stub.Determinate(image_iterator)
        os.makedirs(result_dir, exist_ok=True)
        result_list = []
        (result_dir / image_path.parent.name).mkdir(exist_ok=True, parents=True)
        for idx, output_item in enumerate(grpc_output.items):
            if output_item.class_id <= 12:
                
                temp_list = []
                temp_list.append(Path(image_path.parent.name) / image_path.name)
                with open(result_dir / str(image_path.parent.name) / f"{image_path.stem}_{idx:02d}.png", "wb") as f:
                    f.write(output_item.bitmask)
                temp_list.append(result_dir / str(image_path.parent.name) / f"{image_path.stem}_{idx:02d}.png")
                temp_list.append(output_item.class_id + 1)
                temp_list.append(f"{output_item.confidence:.06f}")
                
                result_list.append(list(map(str, temp_list)))
        return result_list



def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="182.162.19.25", help='grpc ip')
    parser.add_argument("--port", type=int, default=50100, help='grpc port')
    parser.add_argument("--image_dir", type=str, default="/DATA1/stylebot_wild_segmentation/stylebot_tta_test", help='grpc port')
    parser.add_argument("--bitmask_dir", type=str, default="./bitmask", help='bitmask_dir')
    parser.add_argument("--csv_path", type=str, default="result.csv", help='csv_path')

    args = parser.parse_args()
    return args



if __name__ == '__main__':
    args = get_args()
    client = Client(ip=args.ip, port=args.port)
    
    image_dir = Path(args.image_dir)
    image_list = list(image_dir.glob("**/*.jpg")) + list(image_dir.glob("**/*.png"))


    with open(args.csv_path, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(CSV_COLUMN_FORMAT)
    
    for image_path in tqdm(image_list[:], total=len(image_list)):
        columns = client.get_service(image_path, args.bitmask_dir)
        with open(args.csv_path, 'a') as f:
            writer = csv.writer(f)
            writer.writerows(columns)
