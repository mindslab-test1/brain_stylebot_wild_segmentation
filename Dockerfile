FROM docker.maum.ai:443/brain/vision:cu101-torch160
ENV LANG ko_KR.UTF-8
ENV LANGUAGE ko_KR.UTF-8
EXPOSE 50100

COPY . /stylebot_seg
WORKDIR /stylebot_seg
RUN git clone https://github.com/facebookresearch/detectron2.git detectron2_repo

RUN pip install torchvision==0.7.0
RUN pip install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu101/torch1.6/index.html
RUN pip install grpcio
RUN pip install grpcio-tools
