import sys
from proto.segmentation_pb2_grpc import ImageSegServicer, add_ImageSegServicer_to_server
from proto.segmentation_pb2 import Point, BoundingBox, SegItem, SegResult

from module import Model
import cv2
import grpc
import numpy as np
import torch

import argparse
import time
import io
import logging
import json
import os
from concurrent import futures

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
CHUNK_SIZE = 1024 * 1024


def save_chunks_to_file(byte_array, filename):
    print("Saved file in server_db -> " + filename)
    with open(filename, 'wb') as f:
        f.write(byte_array)


class ImageSegServicerImpl(ImageSegServicer):
    def __init__(self, yaml_path="detectron2_repo/projects/PointRend/configs/InstanceSegmentation/pointrend_rcnn_R_50_FPN_1x_coco.yaml",
                 weight_path="output/model_final.pth", gpus=None):
        # TODO: select gpus as torch.device
        self.model = Model(yaml_path=yaml_path, weight_path=weight_path)

    def Determinate(self, image_iterator, context):
        try:
            image_data = bytearray()
            for input_data in image_iterator:
                if input_data.image is not None:
                    image_data.extend(input_data.image)

            image_data = bytes(image_data)
            logging.debug('request:%s byte', len(image_data))
            image_np = cv2.imdecode(np.frombuffer(image_data, dtype=np.uint8), cv2.IMREAD_COLOR)
            outputs = self.model.get_segmentation_result(image_np)

            grpc_item_list = []
            for output in outputs:
                item_dict_list = self.model.convert_output_dict(output)
                for item_dict in item_dict_list:
                    item_instance = self._convert_dict_to_instance(item_dict, *image_np.shape[:2])
                    grpc_item_list.append(item_instance)

            return SegResult(items=grpc_item_list)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @staticmethod
    def _convert_dict_to_instance(item_dict, im_height, im_width):
        min_x, min_y, max_x, max_y = item_dict['box'].astype(np.int32)

        bitmask_bool_np = item_dict['mask'].reshape((im_height, im_width))
        bitmask_image = np.zeros((im_height, im_width, 1), dtype=np.uint8)
        bitmask_image[bitmask_bool_np, :] = 255
        mask_bytes = cv2.imencode('.jpg', bitmask_image)[1].tostring()
        left_top = Point(x=min_x, y=min_y)
        right_bottom = Point(x=max_x, y=max_y)
        bbox = BoundingBox(left_top=left_top, right_bottom=right_bottom)
        grpc_output = SegItem(class_id=item_dict['cls_id'],
                           bitmask=mask_bytes,
                           box=bbox,
                           confidence=item_dict['score'])

        return grpc_output


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", type=int, default=50100, help='grpc port')
    parser.add_argument('--log_level', type=str, default='INFO', help='logger level')
    parser.add_argument('--yaml_path', type=str,
                        default='detectron2_repo/projects/PointRend/configs/InstanceSegmentation/pointrend_rcnn_R_50_FPN_1x_coco.yaml', help='config file')
    parser.add_argument('--checkpoint_path', type=str,
                        default='pretrained/model_final.pth',
                        help='checkpoint path')
    parser.add_argument('--gpu_id', type=str, default='0', help='gpu id')  # TODO
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = get_args()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    service_server = ImageSegServicerImpl(yaml_path=args.yaml_path, weight_path=args.checkpoint_path, gpus=args.gpu_id)
    servicer = add_ImageSegServicer_to_server(service_server, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()
    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
