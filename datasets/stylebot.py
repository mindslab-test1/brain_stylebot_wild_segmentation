r'''
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    referecend https://github.com/sunggukcha/VSENet/blob/master/datasets/coco.py
'''
# project dependency
try:
    from datasets.custom_transforms import *
except:
    from custom_transforms import *
# library
from PIL import Image
from pycocotools import mask
from pycocotools.coco import COCO
from torch.utils.data import Dataset
from torchvision import transforms

import json
import numpy as np
import os
import random

opj = os.path.join

class Stylebot(Dataset):
    r'''
        args requires:
            basedir (str): basedir
            resolution (pair<int, int>): resize resolution
    '''
    def __init__(self, args, split='train'):
        super().__init__()
        # load images
        ann_file = opj(args.basedir, 'stylebot.json')
        with open(ann_file, 'r') as f:
            stylebot = json.load(f)
        self.ids = list()
        for image in stylebot['images']:
            self.ids.append(image['id'])
        # self variables
        self.split = split
        self.img_dir = args.basedir
        self.resolution = args.resolution
        self.coco = COCO(ann_file)
        self.coco_mask = mask
        self.transforms = dict()
        self.transforms['train'] = lambda x: \
            transforms.Compose([
                Resize(self.resolution, Image.BILINEAR),
                Flip(x),
                Normalize(),
                ToTensor()
            ])
        self.transforms['test'] = lambda x: \
            transforms.Compose([
                Resize(self.resolution, Image.BILINEAR),
                Normalize(),
                ToTensor()
            ])
        self.transforms['label'] = lambda x: \
            transforms.Compose([
                Resize(self.resolution, Image.NEAREST),
                Flip(x),
                ToTensor()
            ])


    def __getitem__(self, index):
        image, label, name = self._make_img_gt_point_pair(index)
        x = (random.random() > 0.5)
        image = self.transforms[self.split](x)(image)
        label = self.transforms['label'](x)(label)
        return {'image': image, 'label': label, 'name': name}

    def _make_img_gt_point_pair(self, index):
        image_id = self.ids[index]
        image_metadata = self.coco.loadImgs(image_id)[0]
        filename = image_metadata['filename']

        image = Image.open(opj(self.img_dir, filename)).convert('RGB')
        cocotarget = self.coco.loadAnns(self.coco.getAnnIds(imgIds=image_id))
        label = Image.fromarray(
            self._gen_seg_mask(cocotarget, image_metadata['height'], image_metadata['width'])
        )
        return image, label, filename

    def _gen_seg_mask(self, target, h, w):
        label = np.zeros((h, w), dtype=np.uint8)
        for instance in target:
            segs = [ seg for seg in instance['segmentation'] if len(seg) > 0 ]
            rle = self.coco_mask.frPyObjects(segs, h, w)
            m = self.coco_mask.decode(rle)
            c = int(instance['category_id'])
            if len(m.shape) < 3:
                label[:, :] += (label == 0) * (m * c)
            else:
                label[:, :] += (label == 0) * (((np.sum(m, axis=2)) > 0) * c).astype(np.uint8)
        return label

if __name__ == '__main__':
    from omegaconf import OmegaConf
    from visualize import Visualize
    class Args:
        pass
    args = Args()
    args.basedir = '../../../datasets/stylebot_test'
    #args.basedir = '/home/sungguk/datasets/stylebot_2nd/stylebot.json'
    args.resolution = (513, 513)
    visualize = Visualize('stylebot')
    palette = visualize.palette
    stylebot = Stylebot(args, split='test')
    for sample in stylebot:
        image, label, name = sample['image'], sample['label'], sample['name']
        if False:
            image = np.asarray(image)
            label = np.asarray(label)
            name = name.split('/')[-1]
            visualize.predict_color([label], [image],[name])
            continue
        # origin
        origin = "../" + name
        name = name.split('/')[-1]
        origin = Image.open(origin)
        origin = np.asarray(origin)
        # label
        label = np.asarray(label)
        label = Image.fromarray(label)
        label = label.resize(origin.shape[:2][::-1], Image.NEAREST)
        label = np.asarray(label)
        print(np.unique(label))
        # result and mask
        result = np.zeros(origin.shape)
        mask = np.array(palette[label.squeeze()])
        result[label==0] = origin[label==0]
        result[label!=0] = origin[label!=0] /2 + mask[label!=0] /2
        result = Image.fromarray(result.astype(np.uint8))
        result.save(opj('test', name))
